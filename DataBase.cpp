#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include "DataBase.h"
#include <algorithm>
#include <string.h>
#include "md5.h"
#include <time.h>
#include <iostream>
#include <sstream>
#include <array>
#include <set>
bool findSQLinjection(std::string check)
{
	if ((check.find("=") != std::string::npos) || (check.find(";") != std::string::npos))//check for sql injection
		return true;
	else return false;
}
std::string EncodePassword(std::string password)
{
	std::string output = md5(password);
	//std::cout << output << std::endl; FOR TESTING
	return output;
}
DataBase::DataBase()
{
	int ret;
	ret = sqlite3_open(database_name.c_str(), &db);
	if (ret)
		throw OpenDatabaseException();
}
DataBase::~DataBase()
{
	sqlite3_close(db);
}
static int usersListCallback(void *users_vector, int argc, char **argv, char **azColName)
{
	int i;
	for (i = 0; i < argc; i++)
	{
		((std::vector<std::string>*)users_vector)->push_back(argv[i]);//converts the vector passed as a void*
	}
	return 0;
}
void PrintString(std::string i) 
{
	std::cout << i << std::endl;
}
static int IsTrueCallback(void* ret_bool, int argc, char**argv, char**azColName)
{
	if (argc == 0)
		*(bool*)ret_bool = false;
	else *(bool*)ret_bool = true;
	return 0;
}
bool DataBase::isUserExists(std::string username)
{
	int ret;
	char* errormsg;
	bool IsExists = false;
	std::string command = "select username from t_users where username = \"";
	if (findSQLinjection(username))
		return false;
	command += username;
	command += "\";";
	ret = sqlite3_exec(db, command.c_str(), IsTrueCallback, &IsExists, &errormsg);//check wether the user exists or not
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	//for_each(users.begin(), users.end(), PrintString);
	return IsExists;
}
bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	int ret;
	char* errormsg;
	bool returner = false;
	if (findSQLinjection(username))
		return false;
	std::string encoded_password = EncodePassword(password);
	// username = "\"1 OR \"1\" = \"1 SQL INJECTION!!!
	std::string command = "select * from t_users where username = \"" + username + "\" and password = \"" + encoded_password + "\";";
	ret = sqlite3_exec(db, command.c_str(), IsTrueCallback,&returner, &errormsg);
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	return returner;
}
bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	int ret;
	char* errormsg;
	if (findSQLinjection(username) || findSQLinjection(email))
		return false;
	std::string encoded_password = EncodePassword(password);
	std::string command = "insert into t_users(username,password,email) values(\"" + username + "\",\"" + encoded_password + "\",\"" + email + "\");";
	ret = sqlite3_exec(db, command.c_str(), NULL, NULL, &errormsg);
	if (ret)//if it doesnt equal 0, there is an error
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
		return false;
	}
	else return true;//no need put the else statement, but it makes it look cooler.
}
int QuestionsCallback(void* length, int argc, char **argv, char **azColName)
{
	//std::cout << "questionscallback" <<  argv[0] << std::endl;
	int* i = (int*)length;
	*i = atoi(argv[0]);//need to tell it that it's an int pointer for some reason
	return 0;
}
int RecursiveRandomizing(std::vector<int>* usedNums,int range)//recursive function to generate random numbers, while avoiding used numbers
{
	static int i;
	i++;
	if (i == 1)//to avoid using the same seed multiple times
	{
		srand(time(NULL));
	}
	int newnum = rand() % range + 1;
	if (std::find(usedNums->begin(), usedNums->end(), newnum) != usedNums->end())
		RecursiveRandomizing(usedNums, range);
	else
		return newnum;
}
static int GetQuestionsCallback(void* questions_vector, int argc, char **argv, char **azColName)
{
	std::vector<Question*>* ret = (std::vector<Question*>*)questions_vector;
	struct SetUpQuestion //need to have a struct to avoid having getters in the Question class. this way we can input the question data at once
	{
		int id;
		std::string question;
		std::string correctAnswer;
		std::string ans2;
		std::string ans3;
		std::string ans4;
	};
	Question* q;
	SetUpQuestion setter;
	for (int i = 0; i < argc; i++)
	{
		switch ((i+1) % 6)
		{
		case 1:
			setter.id = atoi(argv[i]);
			break;
		case 2:
			setter.question = argv[i];
			break;
		case 3:
			setter.correctAnswer = argv[i];
			break;
		case 4:
			setter.ans2 = argv[i];
			break;
		case 5:
			setter.ans3 = argv[i];
			break;
		case 0:// as in 6, as in i == 5
			setter.ans4 = argv[i];
			q= new Question(setter.id, setter.question, setter.correctAnswer, setter.ans2, setter.ans3, setter.ans4);
			ret->push_back(q);
			break;
		default:
			std::cout << "this is not a normal message.this is an error" << std::endl;
		}
		/*std::cout << azColName[i] << ": ";
		std::cout << argv[i] << std::endl;*/
	}
	
	return 0;
}
std::vector<Question*> DataBase::initQuestions(int QuestionsNum)
{
	std::string getAmmountOfQuestionsInDb("SELECT COUNT(*) FROM t_questions;");// the sql command to get the ammount of questions the database contains
	char* errormsg;//the error to return from the exec command
	int length,ret;// ints. length = ammount of questions in the db. ret = is there an error or not?
	std::vector<int> usednums;// the vector to hold the used numbers
	int* questionIDs = new int[QuestionsNum];//the array to hold the question ids.
	ret = sqlite3_exec(db, getAmmountOfQuestionsInDb.c_str(), QuestionsCallback, &length, &errormsg);//get the ammouint of quesions in the database into length
	if (ret)
	{
		sqlite3_free(errormsg);
		delete[] questionIDs;
		throw RetrieveDatabaseException();
	}
	//std::cout << length << std::endl;//FOR TESTING
	for (int i = 0; i < QuestionsNum; i++)//loop to generate the question ids
	{
		questionIDs[i] =RecursiveRandomizing(&usednums, length);//get a random id
		usednums.push_back(questionIDs[i]);//push that id into the 'usedids'
	}
	std::string GetQuestionsFromDB("select * from t_questions where ");
	for (int i = 0; i < QuestionsNum; i++)//loop to build the sql command
	{
		GetQuestionsFromDB.append("question_id = ");
		std::ostringstream converter;
		converter << questionIDs[i];
		GetQuestionsFromDB.append(converter.str());
		if (i == (QuestionsNum - 1))
			GetQuestionsFromDB += ";";
		else
			GetQuestionsFromDB += " or ";
	}
	//std::cout << GetQuestionsFromDB << std::endl;
	std::vector<Question*> questions_vector;
	ret = sqlite3_exec(db, GetQuestionsFromDB.c_str(), GetQuestionsCallback, &questions_vector, &errormsg);
	if (ret)
	{
		sqlite3_free(errormsg);
		delete[] questionIDs;
		throw RetrieveDatabaseException();
	}
	delete[] questionIDs;
	return (questions_vector);
}
int InsertGameCallback(void* ret, int argc, char **argv, char **azColName)
{
	*(int*)ret = atoi(argv[0]);
	return 0;
}
int DataBase::insertNewGame()
{
	int id;
	char* errormsg;
	std::string InsertGame("insert into t_games(status,start_time,end_time)values(0,CURRENT_TIMESTAMP,NULL);SELECT last_insert_rowid();");
	int ret = sqlite3_exec(db, InsertGame.c_str(), InsertGameCallback, &id, &errormsg);
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	return id;
}
bool DataBase::UpdateGameStatus(int id)
{
	std::string UpdateGame("update t_games set status=1, end_time=CURRENT_TIMESTAMP where game_id=");
	std::ostringstream converter;
	char* errormsg;
	converter << id << ";";
	UpdateGame.append(converter.str());
	//std::cout << UpdateGame << std::endl;
	int ret = sqlite3_exec(db, UpdateGame.c_str(), NULL, NULL, &errormsg);
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
		return false;
	}
	else return true;
}
bool DataBase::AddAnswerToPlayer(int game_id, std::string username, int question_id, std::string answer, bool is_correct, int answer_time)
{
	char* errormsg;
	std::string AddAnswer("insert into t_players_answers(game_id,username,question_id,player_answer,is_correct,answer_time)values(");
	std::ostringstream inputer;
	inputer << game_id << ", \"" << username << "\", " << question_id << ", \"" << answer << "\", " << is_correct << ", " << answer_time << ");";
	AddAnswer.append(inputer.str());
	int ret = sqlite3_exec(db, AddAnswer.c_str(), NULL, NULL, &errormsg);
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
		return false;
	}
	else return true;

}
int CorrectAnswersCallback(void* ret, int argc, char **argv, char **azColName)
{
	*(int*)ret = atoi(argv[0]);
	return 0;
}
std::multimap<int,std::string>  DataBase::GetBestScores()//most correct answers all time
{
	int ret,CorrectAnswers;
	char* errormsg;
	std::vector<std::string> users;
	std::multimap<int, std::string> mapa; // the map to return
	std::string command = "select username from t_users";
	std::string AmmountCommand = "select count(*) from t_players_answers where is_correct = 1 and username = ";
	ret = sqlite3_exec(db, command.c_str(), usersListCallback, &users, &errormsg);//get a list of users
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	for (auto user : users)//iterate over every user. for every user we need to see how many answers he has 
	{
		std::string temp = AmmountCommand + "\""+user + "\";";
		ret = sqlite3_exec(db, temp.c_str(), CorrectAnswersCallback, &CorrectAnswers, &errormsg);//get the current users all time answers
		if (ret)
		{
			sqlite3_free(errormsg);
			throw RetrieveDatabaseException();
		}
		mapa.insert(std::pair<int, std::string>(CorrectAnswers,user));//push the username + score into the temporary map
	}
	auto it = mapa.end();
	std::advance(it, -3);
	mapa.erase(mapa.begin(),it);
	return mapa;
}
int WrongAnswersCallback(void* ret, int argc, char **argv, char **azColName)
{
	int * i = (int*)ret;
	*i = atoi(argv[0]);
	return 0;
}
int CountGamesCallback(void* ret, int argc, char **argv, char **azColName)
{
	((std::set<int>*)ret)->insert(atoi(argv[0]));
	return 0;
}
int AverageAnswerCallback(void* ret, int argc, char **argv, char **azColName)//the return value needs to be average *100
{
	int times=0;
	((std::vector<int>*)ret)->push_back(atoi(argv[0]));
	return 0;
}
std::vector<int> DataBase::GetPersonalStatus(std::string username)
{
	//get number of correct and incorrect answers
	int ret, NumCorrectAnswers, NumWrongAnswers;
	char* errormsg;
	std::vector<int> status;
	std::string code = "select count(*) from t_players_answers where is_correct = 1 and username = \"" + username + "\";";//correct answers
	ret = sqlite3_exec(db, code.c_str(), CorrectAnswersCallback, &NumCorrectAnswers, &errormsg);//get the current users all time answers
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	code = "select count(*) from t_players_answers where is_correct = 0 and username = \"" + username + "\";";//incorrect answers
	ret = sqlite3_exec(db, code.c_str(), CorrectAnswersCallback, &NumWrongAnswers, &errormsg);//get the current users all time Wrong answers
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	//get number of games
	code = "select game_id from t_players_answers where username = \"" + username + "\";";
	std::set<int> GamesSet;
	ret = sqlite3_exec(db, code.c_str(), CountGamesCallback, &GamesSet, &errormsg);//get the current users all time answers
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	status.push_back(GamesSet.size());
	status.push_back(NumCorrectAnswers);
	status.push_back(NumWrongAnswers);
	//get average time
	code = "select answer_time from t_players_answers where username = \"" + username + "\";";
	std::vector<int> times;
	ret = sqlite3_exec(db, code.c_str(), AverageAnswerCallback, &times, &errormsg);//get the current users all time answers
	if (ret)
	{
		sqlite3_free(errormsg);
		throw RetrieveDatabaseException();
	}
	int avg,total=0;
	for (auto ii : times)
		total += ii;//ii is the int. it should go through the whole vector
	float RealAvg = (float)total / times.size();
	avg = RealAvg * 100;
	status.push_back(avg);
	return status;
}
