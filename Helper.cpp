#include "Helper.h"
#include "User.h"

#include <string>
#include <iomanip>
#include <sstream>

using namespace std;
std::string Helper::buildSendMessage(std::vector<std::string> vals)
{
	std::string ret = "";
	for (auto s : vals)
	{
		ret += s;
	}
	return ret;
}
// recieves the type code of the message from socket (first byte)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Helper::getMessageTypeCode(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 3);
	std::string msg(s);

	if (msg == "")
		return 0;

	int res = std::atoi(s);
	delete s;
	return  res;
}
bool Helper::doesCodeExist(int code)
{
	return (code == 200 ||  code == 201 || code == 203 || code == 205 || code == 207 || code == 209 || code == 211 || code == 213 || code == 215 || code == 217 || code == 219 || code == 222 || code == 223 || code == 225 || code == 299);
}
// send data to socket
// this is private function
void Helper::sendData(SOCKET sc, std::string message) 
{
	const char* data = message.c_str();
	
	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

int Helper::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s= getPartFromSocket(sc, bytesNum, 0);
	return atoi(s);
}

string Helper::getStringPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum, 0);
	string res(s);
	return res;
}

// recieve data from socket according byteSize
// this is private function
char* Helper::getPartFromSocket(SOCKET sc, int bytesNum)
{
	return getPartFromSocket(sc, bytesNum, 0);
}

char* Helper::getPartFromSocket(SOCKET sc, int bytesNum, int flags)
{
	if (bytesNum == 0)
	{
		return "";
	}

	char* data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}


string Helper::getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr; 
	ostr <<  std::setw(digits) << std::setfill('0') << num;
	return ostr.str();

}
RecievedData* Helper::getData(SOCKET skt)
{
	char* d1 = getPartFromSocket(skt, 1024);
	std::string data = std::string(d1);
	std::string codeAsString = data.substr(0, 3);
	int code = stoi(codeAsString);
	data = data.substr(3, std::string::npos);
	RecievedData* ret = new RecievedData(code, data);
	return ret;
}