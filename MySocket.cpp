#include <condition_variable>
#include <string>
#include <vector>
#include "MySocket.h"
/*
	std::string username;5
	SOCKET skt;
	char recv_buffer[2048];
	*/
#pragma comment (lib,"ws2_32.lib")

#define DEFAULT_PORT "27015"

int MySocket::connect()//initialize skt(socket)
{
	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;
	SOCKET skt = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 0), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	ListenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ListenSocket == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	::bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
	

	freeaddrinfo(result);

	iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// Accept a client socket
	skt = accept(ListenSocket, NULL, NULL);
	if (skt == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}

	// No longer need server socket
	closesocket(ListenSocket);
}

int MySocket::connect(SOCKET listenSocket)//initialize skt(socket)
{

	SOCKET skt = INVALID_SOCKET;

	// Accept a client socket
	skt = accept(listenSocket, NULL, NULL);
	if (skt == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(listenSocket);
		WSACleanup();
		return 1;
	}

}



MySocket::~MySocket()
{
	closesocket(skt);
	WSACleanup();
}

void MySocket::send_data(std::string msg)
{
	const char* data = msg.c_str();
	send(skt, data, msg.size(), 0);
}
void MySocket::recv_data()
{
	recv(skt, recv_buffer, 2048, 0);
}
SOCKET MySocket::get_Socket()
{
	return skt;
}
void MySocket::set_Socket(SOCKET sock)
{
	skt = sock;
}