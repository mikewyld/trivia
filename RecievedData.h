#ifndef RecievedData_H
#define RecievedData_H

#include <string>
#include <iostream>
class RecievedData
{
public:
	int code;
	std::string data;

	RecievedData(int msgCode, std::string rcvData)
	{
		code = msgCode;
		data = rcvData;
	}

};
#endif
