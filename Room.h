#ifndef Room_H
#define Room_H

#include <iostream>
#include <condition_variable>
#include <string>
class User;
class Game;
class Room {
	User* admin;

public: 
	std::string name; // room name
	int questionTime; //in seconds

	int id;//this is an id, hail hydra
	int maxUsers;

	int questionNumber;
	std::vector<User*> users;
	Room(std::vector<User*> users, int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime);
	std::string getUsersAsString(std::vector<User*> users, User* excludeUser);
	void sendMessage(User* excludeUser, std::string message);
	void sendMessage(std::string message);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);


};
#endif