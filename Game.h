#ifndef Game_H
#define Game_H

#include <condition_variable>
#include <string>
#include <map>
#include <vector>
#include "DataBase.h"
#include <iostream>
class User;
class Room;
class Game 
{
public:
	std::map<std::string, int> results;//im the map im the map im the map im the map im the map
	//string - player username, int - player score in the game
	int currentTurnAnswers;
	std::vector<Question*> questions;
	std::vector<User*> players;
	int questionsNo;
	DataBase& db;
	int currQuestionIndex;
	int id;
	Game(const std::vector<User*>& players, int questionsNo, DataBase& _db);
	~Game();
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFIrstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);

//this comment has no use in the entirety of the observable universe
};
#endif
