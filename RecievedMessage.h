#ifndef RecievedMessage_H
#define RecievedMessage_H

#include <condition_variable>
#include <string>
#include <map>
#include <WinSock2.h>
#include "User.h"

#include <vector>
#include <iostream>
class RecievedMessage
{
	SOCKET sock;
	User* user;
	int messageCode;
	std::vector<std::string> values;

public:
	RecievedMessage::RecievedMessage(int code, std::vector<std::string> values, SOCKET sock);
	RecievedMessage(int code, User* user);
	RecievedMessage(int code, User* user, std::vector<std::string> values);
	SOCKET get_sock();
	User* get_user();
	void set_user(User* user);
	int get_messageCode();
	std::vector<std::string> get_values();
};
#endif
