#ifndef TriviaServer_H
#define TriviaServer_H

#include <iostream>
#include <condition_variable>
#include <string>
#include <vector>
#include <map>
#include <WinSock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include "Room.h"
#include "Game.h"
#include "RecievedMessage.h"
#include "SafeQueue.h"
#include "RecievedData.h"

class TriviaServer
{


	std::map<SOCKET, User*> connectedUsers;//may need to change
	std::map<int, Room*> roomsList;
	SafeQueue<RecievedMessage> queRcvMessages;
	int roomIdSequence = 0;// will grow by one each time a room is made
	DataBase db;
	SOCKET skt;
	struct addrinfo* result;
public:
	TriviaServer();
	TriviaServer(const TriviaServer&);
	~TriviaServer();//delete list of rooms and users, close the socket
	void serve();
	User* buildUser(SOCKET client_socket, std::string name);
	void bindAndListen();
	void _accept();
	void clientHandler(SOCKET client_socket);
	Room* getRoomById(int roomId);
	int getIdByRoom(Room* room);
	User* getUserByName(std::string userName);
	User* getUserBySocket(SOCKET user_socket);
	void handleRecievedMessages();
	void safeDeleteUser(RecievedMessage rcvmMssage);
	User* handleSignin(RecievedMessage rcvmMssage);//200
	void handleSignout(RecievedMessage rcvmMssage);//201
	bool handleSignup(RecievedMessage rcvmMssage);//203
	void handleLeaveGame(RecievedMessage rcvmMssage);//222
	void handleStartGame(RecievedMessage rcvmMssage);//217
	void handlePlayerAnswer(RecievedMessage rcvmMssage);//219
	bool handleCreateRoom(RecievedMessage rcvmMssage);//213
	bool handleCloseRoom(RecievedMessage rcvmMssage);//215
	bool handleJoinRoom(RecievedMessage rcvmMssage);//209
	bool handleLeaveRoom(RecievedMessage rcvmMssage);//211
	void handleGetUsersInRoom(RecievedMessage rcvmMssage);//207
	void handleGetRooms(RecievedMessage rcvmMssage);//205
	void handleGetBestScores(RecievedMessage rcvmMssage);//223
	void handleGetPersonalStatus(RecievedMessage rcvmMssage);//225
	void addRecievedMessage(RecievedMessage rcvmMssage);
	void buildRecieveMessage(SOCKET client_socket, RecievedData* rcvData);


};
#endif