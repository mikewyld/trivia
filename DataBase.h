#include <string>
#include <vector>
#include "sqlite3.h"
#include <exception>
#include <iostream>
#include "Question.h"
#include <string.h>
#include "RetrieveDatabaseException.h"
#include"OpenDatabaseException.h"
#include <map>
class DataBase 
{
private:
	std::string database_name = "triviadb.db";
	sqlite3* db;
public:
	
	bool isUserAndPassMatch(std::string username, std::string password);
	std::vector<Question*> initQuestions(int QuestionsNum);
	DataBase();
	~DataBase();
	bool isUserExists(std::string username);
	bool addNewUser(std::string username, std::string password, std::string email);
	int insertNewGame();
	bool UpdateGameStatus(int id);
	bool AddAnswerToPlayer(int game_id, std::string username, int question_id, std::string answer, bool is_correct, int answer_time);
	std::multimap<int, std::string>  GetBestScores();
	std::vector<int> GetPersonalStatus(std::string username);
};