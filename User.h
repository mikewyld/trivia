#ifndef User_H
#define User_H

#include <iostream>
#include <condition_variable>
#include <string>
#include "MySocket.h"
#include <vector>
class Room;
class Game;
class User 
{//this is a class

	//this is an empty line of code

	//this is another one

public://this means it's public, as in not private... basically - public

	MySocket sock;//this is a socket
	Room* currRoom;//this is a room
	Game* currGame;//this is a game
	std::string username;//this is a username
	User(std::string name);//this is a constructor
	void send(std::string message);
	void setGame(Game* gm);
	Game* getGame();
	void clearGame();
	bool createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime);
	bool joinRoom(Room* newRoom);
	void leaveRoom();
	int closeRoom();
	bool leaveGame();
	void clearRoom();
};
#endif
