#include <condition_variable>
#include <string>
#include <vector>
class Question 
{
public:

	int id;
	std::string question;
	int correctAnswer;
	std::string answers[4];


	Question(int id, std::string question, std::string correctAnswer, std::string ans2, std::string ans3, std::string ans4);

};
