
#include <condition_variable>
#include <string>
#include <map>
#include "User.h"
#include "Room.h"
#include "Game.h"
#include <vector>
#include <iostream>
/*
std::map<std::string, int> results;//im the map im the map im the map im the map im the map
int currentTurnAnswers;
std::vector<Question*> questions;
std::vector<User*> players;
int questionsNo;
int currQuestionIndex;

DataBase& db;
	*/
Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& _db)
	:db(_db)
{
	this->currQuestionIndex = 0;

	this->players = players;
	this->questionsNo = questionsNo;

	this->id = db.insertNewGame();

	questions = db.initQuestions(questionsNo);
	for (auto player : players)
	{
		results.insert(std::pair<std::string, int>(player->username, 0));
		player->setGame(this);
	}
}
Game::~Game()
{
	for (auto user : players)
	{
		delete user;
	}
	for (auto q : questions)
	{

		delete q;
	}
}
void Game::sendQuestionToAllUsers()
{
	currentTurnAnswers = 0;
	std::string message = "118";
	Question* q = questions[currQuestionIndex];
	message.append(Helper::getPaddedNumber(3, (q->question.size())));
	message.append(q->question);//this was just randomly "+q->question" so i fixed
	message.append((Helper::getPaddedNumber(3, q->answers[0].size())));
	message.append(q->answers[0]);
	message += ((Helper::getPaddedNumber(3, q->answers[1].size())) + q->answers[1]);
	message.append((Helper::getPaddedNumber(3, q->answers[2].size())) + q->answers[2]);
	message.append((Helper::getPaddedNumber(3, q->answers[3].size())) + q->answers[3]);

	for (auto user : players)
	{
		try {
			user->send(message);
		}
		catch(...)
		{
			std::cout << "error sending question to " << user->username << std::endl;
		}
	}
}
void Game::handleFinishGame()
{
	std::string message = "121";
	message += (char)players.size();

	db.UpdateGameStatus(id);
	for (auto user : players)
	{
		message += Helper::getPaddedNumber(2, user->username.size()) + user->username + Helper::getPaddedNumber(2, results[user->username]);
	}
	for (auto user : players)
	{
		try {
			Helper::sendData(user->sock.get_Socket(), message);
			user->leaveGame();
		}
		catch (...)
		{
			std::cout << "error quitting game: " << user->username << std::endl;
		}
	}
}
void Game::sendFIrstQuestion()
{
	sendQuestionToAllUsers();
}
bool Game::handleNextTurn()
{
	if (players.size() == 0)
	{
		handleFinishGame();
		return false;
	}
	if (players.size() < currentTurnAnswers)//not all players have answered
	{
		return true;
	}
	else if (questionsNo == currQuestionIndex)//last round
	{
		handleFinishGame();
		return (false);
	}
	else //everyone answered, not the last round 
	{
		currQuestionIndex++;
		sendQuestionToAllUsers();
		return true;
	}
}
bool Game::handleAnswerFromUser(User* user, int answerNo, int time)
{
	currentTurnAnswers++;
	
	char correctAns = '0';
	if (answerNo == questions[currQuestionIndex]->correctAnswer)
	{
		results[user->username]++;
		db.AddAnswerToPlayer(id, user->username, questions[currQuestionIndex]->id, (questions[currQuestionIndex]->answers[answerNo]), true,time);
		correctAns = '1';
	}
	else
	{
		db.AddAnswerToPlayer(id, user->username, questions[currQuestionIndex]->id, (questions[currQuestionIndex]->answers[answerNo]), false,time);
	}
	Helper::sendData(user->sock.get_Socket(), "120" + correctAns);
	handleNextTurn();
	if ((players.size() == currentTurnAnswers) && (questionsNo == currQuestionIndex)) //if game over
		return false;
	else return true;// no need for the else clause but its cool
}
bool Game::leaveGame(User* currUser)
{
	 currUser->currGame = nullptr;
	players.erase(std::find(players.begin(), players.end(), currUser));//delete user from playerList
	handleNextTurn();
	if ((players.size() == currentTurnAnswers) && (questionsNo == currQuestionIndex))//if end game
	{
		return false;
	}
	else
	{
		return true;
	}
}


