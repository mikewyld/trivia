#pragma once
#include <iostream>

class RetrieveDatabaseException : public std::exception
{

public:
	virtual const char* what() const throw()
	{
		return "there was an error in retrieving from the database";
	}
};