#include <condition_variable>
#include <string>
#include <vector>
class Validator {
public:
	static bool isLowerLetter(char c)
	{
		return (c >= 'a' && c <= 'z');
	}


	static bool isUpperLetter(char c)
	{
		return (c >= 'A' && c <= 'Z');
	}

	static bool isDigit(char c)
	{
		return (c >= '0' && c <= '9');
	}
	static bool isPasswordValid(std::string password)
	{
		if (password.length() < 4 || password.find(' ') != std::string::npos)
		{
			return false;
		}
		int upper = 0, lower = 0, digit = 0;
		for each  (char c in password)
		{
			if (isLowerLetter(c)) { lower++; }
			if (isUpperLetter(c)) { upper++; }
			if (isDigit(c)) { digit++; }
		}

		if (lower == 0 || upper == 0 || digit == 0)
		{
			return false;
		}



		return true;
	}
	static bool isUsernameValid(std::string username)
	{
		if (username == std::string("") || username.find(' ') != std::string::npos)
		{
			return false;
		}
		if (!(isLowerLetter(username[0]) || isUpperLetter(username[0])))
		{
			return false;
		}

		return true;
	}



};
