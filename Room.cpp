#include <vector>
#include <iostream>
#include <condition_variable>
#include <string>
#include "Room.h"
#include "User.h"
#include "Game.h"
#include "Helper.h"

/*
	std::vector<User*> users;
	User* admin;
	int maxUsers;
	int questionTime; //in seconds
	int questionNumber;
	std::string name; // room name
	int id;//this is an id, hail hydra
*/
Room::Room(std::vector<User*> users, int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime)
{
	this->id = id;
	this->admin = admin;
	this->name = name;
	this->maxUsers = maxUsers;
	this->questionNumber = questionsNo;
	this->questionTime = questionTime;

	users.push_back(admin);
}
std::string Room::getUsersAsString(std::vector<User*> users, User* excludeUser)//this will build in the template of the message to the client 108
{
	std::string retval = "";
	retval += (char)users.size();
	for (User* user : users)
	{
		if (user->username != excludeUser->username)
		{
			retval += Helper::getPaddedNumber(2,stoi(user->username)) + user->username;
		}
	}
	return retval;
}
void Room::sendMessage(User* excludeUser, std::string message)
{
	for (User* user : users)
	{
		if (user != excludeUser)
		{
			user->send(message);
		}
	}
}
void Room::sendMessage(std::string message)
{
	sendMessage(nullptr, message);//runs the above function
}
bool Room::joinRoom(User* user)
{
	if (maxUsers <= users.size())// if room is full
	{
		//already sent fail message
		return false;
	}
	users.push_back(user);

	//already told user he joined properly
	
	for (auto _user : users)//now send all users a list of the users in the room
	{
		std::string userList = getUsersAsString(users, nullptr);//this function returns a string in the format of the protocol
		Helper::sendData(_user->sock.get_Socket(), "108" + userList);
	}
	return true;
}
void Room::leaveRoom(User* user)
{
	for (User* _user : users)
	{
		if (_user == user)
		{
			users.erase(std::find(users.begin(), users.end(), user));//erases requested use from vector, UNTESTED
			break;
		}
	}

	//send everyone list of remaining players
	for (auto _user : users)
	{
		std::string userList = getUsersAsString(users, user);//this function returns a string in the format of the protocol
		Helper::sendData(_user->sock.get_Socket(), "108" + userList);
	}
}
int Room::closeRoom(User* user)
{
	if (user != admin)
	{
		return -1;
	}

	for (User* _user : users)
	{
		_user->clearRoom();
	}
	return 0;
}