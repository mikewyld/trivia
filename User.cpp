#include <condition_variable>
#include <string>
#include <vector>
#include "User.h"
#include "Room.h"
#include "Game.h"
#include <stdio.h>
#include <time.h>
/*
std::string username;
Room* currRoom;
Game* currGame;
MySocket sock;
*/

User::User(std::string name)
{
	username = name;
	currRoom = nullptr;
	currGame = nullptr;
}
void User::send(std::string message)
{
	sock.send_data(message);
}
Game* User::getGame()
{
	return currGame;
}
void User::setGame(Game* gm)
{
	currRoom = nullptr;
	currGame = gm;
}
void User::clearGame()
{
	currGame = nullptr;
}
bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{

	//Room::Room(std::vector<User*> users, int id, User* admin, std::string name, int maxUsers, int questionsNo, int questionTime)
	std::vector<User*> a;
	currRoom = new Room(a, roomId, this, roomName, maxUsers, questionsNo, questionTime);
	return true;
}
bool User::joinRoom(Room* newRoom)
{
	if (currGame != nullptr)
	{
		return false;
	}
	newRoom->joinRoom(this);
	return true;
}
void User::leaveRoom()
{
	currRoom->leaveRoom(this);
	currRoom = nullptr;
}
int User::closeRoom()
{
	if (currRoom->closeRoom(this) != -1)//success
	{
		delete currRoom;
		currRoom = nullptr;
		return 1;
	}
	return -1;
}
bool User::leaveGame()
{
	if (((currGame->players.size()) == currGame->currentTurnAnswers) && (currGame->questionsNo == currGame->currQuestionIndex)) //if game over
	{
		currGame = nullptr;
		return true;
		currGame->leaveGame(this);
		currGame = nullptr;
		return false;

	}
	else return true;//again not nesceesary
}
void User::clearRoom()
{
	currRoom = nullptr;
}