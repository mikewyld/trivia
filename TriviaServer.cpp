#include <vector>
#include <iostream>
#include <condition_variable>
#include <thread>
#include <string>
#include "TriviaServer.h"
#include "Helper.h"
#include "Validator.h"
#include <WinSock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
/*
std::map<SOCKET, User*> connectedUsers;//may need to change
std::map<int, Room*> roomsList;
SafeQueue<RecievedMessage> queRcvMessages;
int roomIdSequence = 0;// will grow by one each time a room is made
DataBase db;
SOCKET skt;

*/
#pragma comment (lib,"ws2_32.lib")


TriviaServer::TriviaServer(const TriviaServer&)
{
	
}
TriviaServer::TriviaServer() : db()
{
	//this->db = DataBase();

	static int i = 0;
	i++;
	//std::thread th(&TriviaServer::handleRecievedMessages, TriviaServer());//start a thread to loop over the recieved messages and handle them
	std::thread th(std::thread(&TriviaServer::handleRecievedMessages, this));
	th.detach();
	WSADATA wsaData;
	int iResult;

	//struct addrinfo *result = NULL;
	struct addrinfo hints;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 0), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		//throw exception
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	char* portnum = "8820\0";
	iResult = getaddrinfo(NULL, (PCSTR)portnum, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		//throw exception
	}

	// Create a SOCKET for connecting to server
	skt = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (skt == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		//throw exception
	}
	serve();
}


TriviaServer::~TriviaServer()//delete list of rooms and users, close the socket
{
	for (auto user : connectedUsers)
	{
		delete user.second;
	}

	for (auto room : roomsList)
	{
		delete room.second;
	}
	delete result;
	closesocket(skt);
	WSACleanup();
}
void TriviaServer::serve()
{
	bindAndListen();
	while (true)
	{
		std::cout << "waiting for new client..." << std::endl;
		_accept();
	}
}
void TriviaServer::bindAndListen()
{
	// Setup the TCP listening socket
	::bind(skt, result->ai_addr, (int)(result->ai_addrlen));


	freeaddrinfo(result);

	int iResult = listen(skt, SOMAXCONN);

	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(skt);
		WSACleanup();
		//throw exception
	}
}
void TriviaServer::_accept()
{
	SOCKET sokt = INVALID_SOCKET;

	// Accept a client socket
	sokt = accept(skt, NULL, NULL);//sokt is our clients socket
	if (sokt == INVALID_SOCKET) {
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(skt);
		WSACleanup();
		//throw exception
	}

	connectedUsers.insert(std::pair<SOCKET, User*>(sokt, nullptr));//this is a problem. you can't put in a nullptr.
	std::thread th(&TriviaServer::clientHandler, this, sokt);
	th.detach();

}
void TriviaServer::clientHandler(SOCKET client_socket)
{
	while (true)
	{
		RecievedData* rcvData = Helper::getData(client_socket);
		if (rcvData->code == 0 || rcvData->code == 299)
		{
			break;
		}
		buildRecieveMessage(client_socket, rcvData);
		delete rcvData;
	}
	//here we add a message "from the user" to end the communication with him
	RecievedMessage endCommunication(299, getUserBySocket(client_socket));
	queRcvMessages.enqueue(endCommunication);
}

Room* TriviaServer::getRoomById(int roomId)
{
	return roomsList[roomId];
}

int TriviaServer::getIdByRoom(Room* room)
{
	for (auto set : roomsList)
	{
		if (set.second == room)
		{
			return set.first;
		}
	}
	/*if couldnt find then*/ return -1;
}
User* TriviaServer::getUserByName(std::string userName)
{
	for (auto u : connectedUsers)
	{
		if (u.second == nullptr)//need a seperate "if" in order to not try to acsess nullptr->username (non exsitent)
			return nullptr;
		if ((u.second)->username == userName)
			return u.second;
	}
	return nullptr;//user not found
}
User* TriviaServer::getUserBySocket(SOCKET user_socket)
{
	return connectedUsers[user_socket];
}
void TriviaServer::handleRecievedMessages()
{
	while (true)
	{
		if (queRcvMessages.size() > 0)
		{
			RecievedMessage rcv = queRcvMessages.dequeue();
			if (rcv.get_messageCode() == 299 || !Helper::doesCodeExist(rcv.get_messageCode()))
			{
				safeDeleteUser(rcv);
			}
			switch (rcv.get_messageCode())
			{
			case 200:
				handleSignin(rcv);
				break;
			case 201:
				handleSignout(rcv);
				break;
			case 203:
				handleSignup(rcv);
				break;
			case 205:
				handleGetRooms(rcv);
				break;
			case 207:
				handleGetUsersInRoom(rcv);
				break;
			case 209:
				handleJoinRoom(rcv);
				break;
			case 211:
				handleLeaveRoom(rcv);
				break;
			case 213:
				handleCreateRoom(rcv);
				break;
			case 215:
				handleCloseRoom(rcv);
				break;
			case 217:
				handleStartGame(rcv);
				break;
			case 219:
				handlePlayerAnswer(rcv);
				break;
			case 222:
				handleLeaveGame(rcv);
				break;
			case 223:
				handleGetBestScores(rcv);
				break;
			case 225:
				handleGetPersonalStatus(rcv);
				break;
			
			}

		}
	}
}
void TriviaServer::safeDeleteUser(RecievedMessage rcvmMssage)
{
	handleSignout(rcvmMssage);
	closesocket(rcvmMssage.get_sock());
}
User* TriviaServer::handleSignin(RecievedMessage rcvmMssage)//200
{
	std::string name = rcvmMssage.get_values()[0];
	std::string pass = rcvmMssage.get_values()[1];
	if (!db.isUserExists(name))//if the user doesnt exist. doing this in a separate "if" in order to avoid calling "isUserPassMatch" on invalid username
	{
		Helper::sendData(rcvmMssage.get_sock(), "1021");
		return nullptr;
	}
	if (!(db.isUserAndPassMatch(name, pass))) //if the username doesnt match the password
	{
		Helper::sendData(rcvmMssage.get_sock(), "1021");
		return nullptr;
	}
	if (getUserByName(name) != nullptr)// if the user is already logged in
	{
		Helper::sendData(rcvmMssage.get_sock(), "1022");
		return nullptr;
	}

	User* usr = new User(name);
	usr->sock.set_Socket(rcvmMssage.get_sock());
	connectedUsers.insert(std::pair<SOCKET, User*>(rcvmMssage.get_sock(), usr));
	Helper::sendData(rcvmMssage.get_sock(), "1020");
	return usr;
}
void TriviaServer::handleSignout(RecievedMessage rcvmMssage)//201
{
	if (getUserByName(rcvmMssage.get_user()->username) != nullptr)
	{
		connectedUsers.erase(rcvmMssage.get_sock());
		handleCloseRoom(rcvmMssage);
		handleLeaveGame(rcvmMssage);
		handleLeaveGame(rcvmMssage);
	}
}
bool TriviaServer::handleSignup(RecievedMessage rcvmMssage)//203
{
	std::string name = rcvmMssage.get_values()[0];
	std::string pass = rcvmMssage.get_values()[1];
	std::string Email = rcvmMssage.get_values()[2];
	if (!Validator::isPasswordValid(pass))
	{
		std::cout << "invalid password attempted to sign up" << std::endl;
		Helper::sendData(rcvmMssage.get_sock(), "1041");
		return false;
	}
	if (!Validator::isUsernameValid(name))
	{
		std::cout << "invalid username attempted to sign up" << std::endl;
		Helper::sendData(rcvmMssage.get_sock(), "1043");
		return false;
	}
	if (db.isUserExists(name))
	{
		std::cout << "existing username attempted to sign up" << std::endl;
		Helper::sendData(rcvmMssage.get_sock(), "1042");
		return false;
	}
	db.addNewUser(name, pass, Email);
	User* usr = new User(name);
	usr->sock.set_Socket(rcvmMssage.get_sock());
	connectedUsers.insert(std::pair<SOCKET, User*>(rcvmMssage.get_sock(), usr));

	Helper::sendData(rcvmMssage.get_sock(), "1040");//success

	return true;


}
void TriviaServer::handleLeaveGame(RecievedMessage rcvmMssage)//222
{
	Game* gm = rcvmMssage.get_user()->getGame();
	if (rcvmMssage.get_user()->leaveGame())
	{
		delete gm;
	}
}
void TriviaServer::handleStartGame(RecievedMessage rcvmMssage)//217
{
	try 
	{
		Game* x = new Game((rcvmMssage.get_user()->currRoom->users), (rcvmMssage.get_user()->currRoom->questionNumber), db);
		rcvmMssage.get_user()->setGame(x);
	}
	catch (...)
	{
		std::cout << "failed to create game from user: " << rcvmMssage.get_user()->username << std::endl;
		Helper::sendData(rcvmMssage.get_sock(), "1180");
		return;
	}
	roomsList.erase(getIdByRoom(rcvmMssage.get_user()->currRoom));
	rcvmMssage.get_user()->currGame->sendFIrstQuestion();
}
void TriviaServer::handlePlayerAnswer(RecievedMessage rcvmMssage)//219
{
	Game* usergame = rcvmMssage.get_user()->currGame;
	if (usergame != nullptr)
	{
		if (!usergame->handleAnswerFromUser(rcvmMssage.get_user(), stoi(rcvmMssage.get_values()[0]), stoi(rcvmMssage.get_values()[1])))//if the game has ended
		{
			delete usergame;
		}
	}
}
bool TriviaServer::handleCreateRoom(RecievedMessage rcvmMssage)//213
{
	User* user = rcvmMssage.get_user();
	if (user == nullptr)
	{
		Helper::sendData(rcvmMssage.get_sock(), "1141");
		return false;
	}
		roomIdSequence++;
		if (user->createRoom(roomIdSequence, rcvmMssage.get_values()[0], stoi(rcvmMssage.get_values()[1]), stoi(rcvmMssage.get_values()[2]), stoi(rcvmMssage.get_values()[3])))//if succeeded in making a room
		{
			roomsList.insert(std::pair<int, Room*>(user->currRoom->id, user->currRoom));
			Helper::sendData(rcvmMssage.get_sock(), "1140");
			return true;
		}
	Helper::sendData(rcvmMssage.get_sock(), "1141");
	return false;

}
bool TriviaServer::handleCloseRoom(RecievedMessage rcvmMssage)//215
{
	User* user = rcvmMssage.get_user();
	if (user == nullptr)
	{
		return false;
	}
	if (user->closeRoom() != -1)
	{
		//will send a 116 message to all users connected to the room
		for (auto user : user->currRoom->users)
		{
			Helper::sendData(user->sock.skt, "116");
		}
		roomsList.erase(getIdByRoom(user->currRoom));
		return true;
	}
	return false;
}
bool TriviaServer::handleJoinRoom(RecievedMessage rcvmMssage)//209
{
	User* user = rcvmMssage.get_user();
	
	if (getRoomById(stoi(rcvmMssage.get_values()[0])) == nullptr|| user == nullptr) //if room or user dont exist
	{
		Helper::sendData(rcvmMssage.get_sock(), "1102");
		return false;
	}
	if ((getRoomById(stoi(rcvmMssage.get_values()[0]))->maxUsers) <= getRoomById(stoi(rcvmMssage.get_values()[0]))->users.size())//if room is full
	{
		Helper::sendData(rcvmMssage.get_sock(), "1101");
		return false;

	}
	user->joinRoom(getRoomById(stoi(rcvmMssage.get_values()[0])));
	Helper::sendData(rcvmMssage.get_sock(), "110" + Helper::getPaddedNumber(2, getRoomById(stoi(rcvmMssage.get_values()[0]))->questionNumber) + Helper::getPaddedNumber(2, getRoomById(stoi(rcvmMssage.get_values()[0]))->questionTime));
	//"110" + questions number + question time
	return true;
}
bool TriviaServer::handleLeaveRoom(RecievedMessage rcvmMssage)//211
{
	User* user = rcvmMssage.get_user();
	if (user == nullptr)//if user or room dont exist
	{
		return false;
	}
	user->leaveRoom();
	Helper::sendData(rcvmMssage.get_sock(), "1120");
	return false;

	return true;
}
void TriviaServer::handleGetUsersInRoom(RecievedMessage rcvmMssage)//207
{
	User* user = rcvmMssage.get_user();
	if (user == nullptr || getRoomById(stoi(rcvmMssage.get_values()[0])) == nullptr)
	{
		Helper::sendData(rcvmMssage.get_sock(), "1080");
		return;
	}
	int id = stoi(rcvmMssage.get_values()[0]);
	std::string userList = getRoomById(id)->getUsersAsString(getRoomById(id)->users, nullptr);//this function returns a string in the format of the protocol
	Helper::sendData(rcvmMssage.get_sock(), "108" + userList);
}
void TriviaServer::handleGetRooms(RecievedMessage rcvmMssage)//205
{
	//build message to fit template of : [106 numberOfRooms roomID ## roomName roomID ## roomName�]
	//code - 3 bytes, roomsnumber - 4 bytes, room id - 4 bytes, nameLen - 2 bytes
	std::string message = "106";
	std::string roomsNum = Helper::getPaddedNumber(4, roomsList.size());
	message += roomsNum;
	for (auto room : roomsList)
	{
		std::string Id = Helper::getPaddedNumber(4, room.first);
		std::string roomName = room.second->name;
		std::string nameLen = Helper::getPaddedNumber(2, roomName.size());
		message = message + Id + nameLen + roomName;
	}
	Helper::sendData(rcvmMssage.get_sock(), message);

}
void TriviaServer::handleGetBestScores(RecievedMessage rcvmMssage)//223
{
	multimap<int, std::string> stats = db.GetBestScores();//[username, best score]
	std::string msg = "124";
	//[username size 2, username, best score 6]
	int i = 0;
	for (auto x : stats)
	{
		msg += Helper::getPaddedNumber(2, x.second.size());
		msg += x.first;
		msg += Helper::getPaddedNumber(6, x.first);
		i++;
	}
	for (i = 3-i; i > 0; i--);//if there are less than 3 values we add empty users to the message
	{
		msg += "0000000";
	}
	Helper::sendData(rcvmMssage.get_sock(), msg);

}
void TriviaServer::handleGetPersonalStatus(RecievedMessage rcvmMssage)//225
{
	//returned vector will contain [num of games, num of correct answers, num of incorect answers, avg answer time]
	vector<int> stats = db.GetPersonalStatus(rcvmMssage.get_user()->username);
	if (stats[0] == 0 && stats[1] == 0)
	{
		Helper::sendData(rcvmMssage.get_sock(), "1260000");
	}
	std::string msg = "126";
	msg += Helper::getPaddedNumber(4, stats[0]);//num of games
	msg += Helper::getPaddedNumber(6, stats[1]);//num of correct answers
	msg += Helper::getPaddedNumber(6, stats[2]);//num of incorrect answers
	msg += Helper::getPaddedNumber(4, stats[3]);//avg time, is multiplied by 100 so for example 42.35 is sent as 4235

	Helper::sendData(rcvmMssage.get_sock(), msg);

}
void TriviaServer::addRecievedMessage(RecievedMessage rcvMssage)
{
	queRcvMessages.enqueue(rcvMssage);
}
void TriviaServer::buildRecieveMessage(SOCKET client_socket, RecievedData* rcvData)
{
	User* newUser;
	int len;
	//std::string data = Helper::getStringPartFromSocket(client_socket, 1024);//i believe this is incorrect, im not sure how were supposed to get more infromation, besides the protocol number, in this function
	std::string data = rcvData->data;
	int msgCode = rcvData->code;
	if (msgCode == 200)
	{
		len = atoi((data.substr(0, 2)).c_str());
		std::string uName = data.substr(2, len);
		newUser = new User(uName);
		connectedUsers.insert(std::pair<SOCKET, User*>(client_socket, newUser));
	}
	if (msgCode == 200)
	if (msgCode == 201 || msgCode == 205 || msgCode == 211 || msgCode == 215 || msgCode == 217 || msgCode == 222 || msgCode == 223 || msgCode == 225 || msgCode == 229)
	//all the message codes that dont have any data after them
	{
		RecievedMessage rcvmsg(msgCode, getUserBySocket(client_socket));
		addRecievedMessage(rcvmsg);
	}
	else if (msgCode == 200 || msgCode == 203)
	//codes with a template of two bytes (for size), data, two bytes, data, etc...
	{
		int max;
		if (msgCode == 200)
		{
			max = 2;
		}
		else //203
		{
			max = 3;
		}
		std::vector<std::string> vals;
		int size;
		int starting_location = 0;
		while (max)
		{
			max--;
			size = stoi(data.substr(starting_location, 2));

			vals.push_back(data.substr(starting_location + 2, size));
			starting_location = starting_location + 2 + size;
		}
		//User* newUser = buildUser(client_socket, vals[1]);
		//connectedUsers.insert(std::pair<SOCKET, User*>(client_socket, newUser));
		RecievedMessage rcvmsg(msgCode, vals, client_socket);
		addRecievedMessage(rcvmsg);
	}
	else if (msgCode == 207 || msgCode == 209)
	{
		std::vector<std::string> vals;
		vals.push_back(data.substr(3, 4));
		RecievedMessage rcvmsg(msgCode, getUserBySocket(client_socket), vals);
	}
	else if (msgCode == 213)
	{
		std::string s = "";
		std::vector<std::string> vals;
		int size = stoi(data.substr(3, 2));
		vals.push_back(data.substr(3 + 2, size));
		s += data[3 + 2 + size + 1];//player num
		vals.push_back(s);
		vals.push_back(data.substr(3 + 2 + size + 1, 2));
		vals.push_back(data.substr(3 + 2 + size + 1 + 2, 2));
		RecievedMessage rcvmsg(msgCode, getUserBySocket(client_socket), vals);
	}
	else if (msgCode == 219)
	{
		std::string s = "";
		std::vector<std::string> vals;
		s += data[3];
		vals.push_back(s);
		vals.push_back(data.substr(3+1, 2));
		RecievedMessage rcvmsg(msgCode, getUserBySocket(client_socket), vals);
	}

	else
	{
		std::cout << "Didn't match any message type code!" << std::endl;
	}
}
User* buildUser(SOCKET client_socket, std::string name)
{
	User* usr = new User(name);
	usr->sock.set_Socket(client_socket);
	return usr;
}