#include <iostream>
#include <exception>
#include "TriviaServer.h"

void main()
{
	try {
		TriviaServer* server = new TriviaServer();
	}
	catch (std::exception a)
	{
		std::cout << a.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "an error occured" << std::endl;
	}
	system("pause");
}