#pragma once
#include <iostream>

class OpenDatabaseException : public std::exception
{
	virtual const char* what() const throw()
	{
		return "unable to open database";
	}
};
