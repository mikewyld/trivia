#include <string>
#include <WinSock2.h>
#include "RecievedMessage.h"

#include <vector>
#include <iostream>
/*
	SOCKET sock;
	User* user;
	int messageCode;
	std::vector<std::string> values;
*/
RecievedMessage::RecievedMessage(int code, std::vector<std::string> values, SOCKET sock)
{
	messageCode = code;
	this->sock = sock;
	this->values = values;
}
RecievedMessage::RecievedMessage(int code, User* _user)
{
	messageCode = code;
	user = _user;
	sock = user->sock.get_Socket();
}
RecievedMessage::RecievedMessage(int code, User* user, std::vector<std::string> values)
{
	messageCode = code;
	this->user = user;
	sock = user->sock.get_Socket();
	this->values = values;
}
SOCKET RecievedMessage::get_sock()
{
	return sock;
}
User* RecievedMessage::get_user()
{
	return user;
}
void RecievedMessage::set_user(User* user)
{
	this->user = user;
}
int RecievedMessage::get_messageCode()
{
	return messageCode;
}
std::vector<std::string> RecievedMessage::get_values()
{
	return values;
}
