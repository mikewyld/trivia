#pragma once
#ifndef WIN_SOCK2_H
#define WIN_SOCK2_H
#include <condition_variable>
#include <string>
#include <vector>
#include <WinSock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include "Helper.h"
class MySocket {
public:
	char recv_buffer[2048];
	SOCKET skt;


	int connect();
	int connect(SOCKET listenSocket);
	~MySocket();
	void send_data(std::string msg);
	void recv_data();//will put received data into recv_buffer
	SOCKET get_Socket();
	void set_Socket(SOCKET sock);
};
#endif